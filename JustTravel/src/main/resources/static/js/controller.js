app.controller('yourTravelsController', function($scope, $http) {
    
	$scope.headingTitle = "Twoje podróże";
	
	$http({
		  method: 'GET',
		  url: '/travel/travels/1'
		}).then(function successCallback(response) {
			$scope.travels = response.data;
		  }, function errorCallback(response) {
			  console.info("error" + response.data);
		  });
	
	$scope.openAddTravelModal = function() {
		$scope.showModalAddNew = true;
		$http({
			  method: 'GET',
			  url: '/destination/allcountry' 
			}).then(function successCallback(response) {
				var countryList = response.data;
				
				var dataList = document.getElementById('json-datalist');
				 var input = document.getElementById('ajax');
				 countryList.forEach(function(item) {
				        var option = document.createElement('option');
				        option.value = item;
				        dataList.appendChild(option);
				 });
			  }, function errorCallback(response) {
				  console.info("error przy pobieraniu listy wszystkich panstw dla pola panstwo" + response.data);
			  });
		
	};
	
	$scope.submitAddTravelForm = function(isValid) {
		if(isValid) {
			$scope.showModalAddNew = false;
			  $http({
				  method: 'POST',
				  url: '/travel/add',
				  data: angular.toJson({
					  "country": $scope.countryAdd,
					  "city": $scope.cityAdd, 
					  "dateStart": $scope.startDateAdd, 
					  "dateEnd": $scope.endDateAdd, 
					  "cost": $scope.costAdd,
					  "description" : $scope.descAdd,
					  "userId" : 1})
					  
			  }).then(function successCallback(response) {
				  $http({
					  method: 'GET',
					  url: '/travel/travels/1'
					}).then(function successCallback(response) {
						$scope.travels = response.data;
					  }, function errorCallback(response) {
						  console.info("error" + response.data);
					  });
				  
					console.info("success przy dodawaniu nowej podrozy");
					
					 $scope.countryAdd = ''; 
					 $scope.cityAdd = ''; 
					 $scope.startDateAdd = ''; 
					 $scope.endDateAdd = ''; 
					 $scope.costAdd = '';
					 $scope.descAdd = '';
					
			  },
			  function errorCallback(response) {
				  console.info("error przy dodawaniu nowej podrozy");
			  });
		}
	};

	$scope.cancelAdd = function() {
		  $scope.showModalAddNew = false;
	};
	
//	$scope.saveTravel = function(index){
//		console.log("save index" + index);
//		$scope.travels[index].readonly = !$scope.travels[index].readonly;
//	}
	
	// pokaz/ukryj pole opisu podrozy
	
	$scope.expandSelected=function(x){
	    $scope.travels.forEach(function(val){
	      val.expanded=false;
	    })
	    x.expanded=true;
	}
	
	// edycja
	
	
	var selectedToEdit = -1;
	
	$scope.openEditTravelModal = function(index) {
		$scope.showModalEdit = true;
		selectedToEdit = index;
		  $scope.countryEdit = $scope.travels[index].destination.country;
		  $scope.cityEdit = $scope.travels[index].destination.city;
		  $scope.startDateEdit = $scope.travels[index].destination.dateStart;
		  $scope.endDateEdit = $scope.travels[index].destination.dateEnd;
		  $scope.costEdit = $scope.travels[index].destination.cost;
		  $scope.descEdit = $scope.travels[index].destination.description;
	};

	$scope.submitEditTravelForm = function(isValid) {
		console.info("edycja");
		if(isValid){
			
			$scope.showModalEdit = false;
			  $scope.travels[selectedToEdit].destination.country = $scope.countryEdit;
			  $scope.travels[selectedToEdit].destination.city = $scope.cityEdit;
			  $scope.travels[selectedToEdit].destination.dateStart = $scope.startDateEdit;
			  $scope.travels[selectedToEdit].destination.dateEnd = $scope.endDateEdit;
			  $scope.travels[selectedToEdit].destination.cost = $scope.costEdit;
			  $scope.travels[selectedToEdit].destination.description = $scope.descEdit;
			  
			  $http({
				  method: 'PUT',
				  url: '/travel/updateTravel',
				  data : $scope.travels[selectedToEdit].destination
				})
				.then(function successCallback(response) {
					console.info("success przy update");
				  },
				  function errorCallback(response) {
					  console.info("error przy update");
				  });
		}
	};

	$scope.cancelEdit = function() {
		  $scope.showModalEdit = false;
	};
	
	// usuwanie 
	
	var selectedToRemove = -1;
	
	$scope.openRemoveTravelModal = function(index){
		selectedToRemove = index;
		$scope.showModalRemoveTravel = true;
		$scope.country = $scope.travels[index].destination.country;
	}
	
	$scope.removeTravel = function(index){
		$http({
			  method: 'DELETE',
			  url: '/travel/remove/' + $scope.travels[selectedToRemove].destination.id
			})
			.then(function successCallback(response) {
				console.info("success przy usuwaniu");
				
				 $http({
					  method: 'GET',
					  url: '/travel/travels/1'
					}).then(function successCallback(response) {
						$scope.travels = response.data;
					  }, function errorCallback(response) {
						  console.info("error" + response.data);
					  });
			  },
			  function errorCallback(response) {
				  console.info("error przy usuwaniu");
			  });
		$scope.showModalRemoveTravel = false;
	};
	
	$scope.cancelRemove = function(){
		$scope.showModalRemoveTravel = false;
	}
	
});

app.controller('mapController', function($scope, $http) {
	
	console.info("wika");
	
	$http({
		  method: 'GET',
		  url: '/destination/map'
		}).then(function successCallback(response) {
			console.info("success" + response.data);
			var cities = response.data;
			
			 for (i = 0; i < cities.length; i++){
		        	console.info("nazwa maista " + cities[i].city);
		            createMarker(cities[i]);
		        }
			
		  }, function errorCallback(response) {
			  console.info("error" + response.data);
		  });
	
//	var cities = [
//        {
//            city : 'Toronto',
//            desc : 'This is the best city in the world!',
//            latitude : 43.7000,
//            longitude : -79.4000
//        },
//        {
//            city : 'New York',
//            desc : 'This city is aiiiiite!',
//            latitude : 40.6700,
//            longitude : -73.9400
//        },
//        {
//            city : 'Chicago',
//            desc : 'This is the second best city in the world!',
//            latitude : 41.8819,
//            longitude : -87.6278
//        },
//        {
//            city : 'Los Angeles',
//            desc : 'This city is live!',
//            latitude : 34.0500,
//            longitude : -118.2500
//        },
//        {
//            city : 'Las Vegas',
//            desc : 'Sin City...\'nuff said!',
//            latitude : 36.0800,
//            longitude : -115.1522
//        }
//    ];
    
    	var mapOptions = {
            zoom: 4,
            center: new google.maps.LatLng(40.0000, -98.0000),
            mapTypeId: google.maps.MapTypeId.TERRAIN
        }

        $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);

        $scope.markers = [];
        
        var infoWindow = new google.maps.InfoWindow();
        
        var createMarker = function (info){
            
            var marker = new google.maps.Marker({
                map: $scope.map,
                position: new google.maps.LatLng(info.latitude, info.longitude),
                title: info.city
            });
            marker.content = '<div class="infoWindowContent">' + info.desc + '</div>' + 'szczegoly';
            
            google.maps.event.addListener(marker, 'click', function(){
                infoWindow.setContent('<h2>' + marker.title + '</h2>' + marker.content);
                infoWindow.open($scope.map, marker);
            });
            
            $scope.markers.push(marker);
            
        }  

        $scope.openInfoWindow = function(e, selectedMarker){
            e.preventDefault();
            google.maps.event.trigger(selectedMarker, 'click');
        }
    
});

app.controller('mainController', function($scope, $http) { 

    $http({
      method: 'GET',
      url: '/destination/latest'
    }).then(function successCallback(response) {
        console.info("success pobranie 3 najnowsszych podrozy" + response.data);
       
        $scope.id1 = response.data[0].id;
	        $scope.country1 = response.data[0].country;
			$scope.city1 = response.data[0].city;
			$scope.startDate1 = response.data[0].dateStart;
			$scope.endDate1 = response.data[0].dateEnd;
			
			
		$scope.id2 = response.data[1].id;
			$scope.country2 = response.data[1].country;
			$scope.city2 = response.data[1].city;
			$scope.startDate2 = response.data[1].dateStart;
			$scope.endDate2 = response.data[1].dateEnd;
			
		$scope.id3 = response.data[2].id;
			$scope.country3 = response.data[2].country;
			$scope.city3 = response.data[2].city;
			$scope.startDate3 = response.data[2].dateStart;
			$scope.endDate3 = response.data[2].dateEnd;
        
      }, function errorCallback(response) {
          console.info("error przy pobieraniu 3 najnowszych podrozy na pierwsza strone" + response.data);
      });

	
});

app.controller('detailsController', function($scope, $http, $routeParams) { 

	var id = $routeParams.id;
	
	$http({
		method: "GET",
		url: '/travel/' + id
	}).then(function successCallback(response) {
		console.info("success przy pobraniu szczegolu podrozy" + response.data);
		$scope.name = response.data.user.name;
		$scope.lastname = response.data.user.lastname;
		$scope.age = response.data.user.age;
		$scope.phone = response.data.user.phone;
		$scope.datein = response.data.user.datein;
		
		$scope.country = response.data.destination.country;
		$scope.city = response.data.destination.city;
		$scope.startdate = response.data.destination.dateStart;
		$scope.enddate = response.data.destination.dateEnd;
		$scope.cost = response.data.destination.cost;
		$scope.desc = response.data.destination.description;
		
		$scope.lat = response.data.destination.latitude;
		$scope.lng = response.data.destination.longitude;
		
	  }, function errorCallback(response) {
		  console.info("error przy pobraniu szczegolu podrozy" + response.data);
	  });
	
});

app.controller('miniMapController', function($scope, $http) {
	
	var cities = [
        {
            city : 'Toronto',
            desc : 'This is the best city in the world!',
            latitude : 43.7000,
            longitude : -79.4000
        },
        {
            city : 'New York',
            desc : 'This city is aiiiiite!',
            latitude : 40.6700,
            longitude : -73.9400
        },
        {
            city : 'Chicago',
            desc : 'This is the second best city in the world!',
            latitude : 41.8819,
            longitude : -87.6278
        },
        {
            city : 'Los Angeles',
            desc : 'This city is live!',
            latitude : 34.0500,
            longitude : -118.2500
        },
        {
            city : 'Las Vegas',
            desc : 'Sin City...\'nuff said!',
            latitude : 36.0800,
            longitude : -115.1522
        }
    ];
    
    	var mapOptions = {
            zoom: 4,
            center: new google.maps.LatLng(40.0000, -98.0000),
            mapTypeId: google.maps.MapTypeId.TERRAIN
        }

        $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);

        $scope.markers = [];
        
        var infoWindow = new google.maps.InfoWindow();
        
        var createMarker = function (info){
            
            var marker = new google.maps.Marker({
                map: $scope.map,
                position: new google.maps.LatLng(info.latitude, info.longitude),
                title: info.city
            });
            marker.content = '<div class="infoWindowContent">' + info.desc + '</div>' + 'szczegoly';
            
            google.maps.event.addListener(marker, 'click', function(){
                infoWindow.setContent('<h2>' + marker.title + '</h2>' + marker.content);
                infoWindow.open($scope.map, marker);
            });
            
            $scope.markers.push(marker);
            
        }  
        
        for (i = 0; i < cities.length; i++){
        	console.info("nazwa maista " + cities[i].city);
            createMarker(cities[i]);
        }

        $scope.openInfoWindow = function(e, selectedMarker){
            e.preventDefault();
            google.maps.event.trigger(selectedMarker, 'click');
        }
    
});


app.controller('yourAccountController', function($scope, $http) { 
	
	$scope.showDate = true;
	$scope.showRegister = true;
	$scope.readonlyDate = true;
	$scope.readonlyRegister = true;
	
	 $http({
	      method: 'GET',
	      url: '/user/1'
	    }).then(function successCallback(response) {
	        console.info("success pobieranie usera po id" + response.data);
	       
	        $scope.user = response.data;
	        
	      }, function errorCallback(response) {
	          console.info("error przy pobieraniu usera po id" + response.data);
	      });
	
	

	$scope.openfileDialog = function(){
		console.info("otwieram pliki");
		$("#fileLoader").click();
    }
	
	$scope.editYourDate = function(){
		$scope.showDate = false;
		$scope.readonlyDate = false;
	}
	
	$scope.submitSaveYourDate = function(isValid){
		console.info("update danych " + isValid);
		if (isValid) {
			$scope.showDate = true;
			$scope.readonlyDate = true;
		}
	}
	
	$scope.editYourRegister = function(){
		$scope.showRegister = false;
		$scope.readonlyRegister = false;
	}
	
	$scope.submitSaveYourRegister = function(isValid){
		$scope.showRegister = true;
	}
    
	
});






