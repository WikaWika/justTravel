var app = angular.module('app', ['ngRoute', 'ngResource', 'ui.bootstrap.modal']);
app.config(function($routeProvider) {
	$routeProvider.when('/yourTravels', {
		templateUrl : '/pages/yourTravels.html',
		controller : 'yourTravelsController'
	}).when('/map', {
		templateUrl : '/pages/map.html',
		controller : 'mapController'
	}).when('/about', {
		templateUrl : '/pages/about.html'
	}).when('/details/:id', {
		templateUrl : '/pages/details.html',
		controller : 'detailsController'
	}).when('/main', {
		templateUrl : '/pages/main.html',
		controller : 'mainController'
	}).when('/yourAccount', {
		templateUrl : '/pages/yourAccount.html',
		controller : 'yourAccountController'
	}).otherwise({
		templateUrl : '/pages/main.html',
		controller : 'mainController'
	});
});

