CREATE TABLE IF NOT EXISTS destination (
	id BIGSERIAL PRIMARY KEY,
	country TEXT,
    city TEXT,
    date_start TIMESTAMP without time zone,
    date_end TIMESTAMP without time zone,
    description TEXT,
    longitude   DECIMAL(11, 8),
    latitude    DECIMAL(10, 8),
    cost TEXT
);

CREATE TABLE IF NOT EXISTS users (
	id BIGSERIAL PRIMARY KEY,
	name TEXT,
    lastname TEXT,
    login TEXT,
    password TEXT,
    birthDate TIMESTAMP without time zone,
    city TEXT,
    country TEXT,
    phone TEXT
);

CREATE TABLE IF NOT EXISTS travel (
	id BIGSERIAL PRIMARY KEY,
	destination_id bigint REFERENCES destination(id),
    user_id bigint REFERENCES users(id),
    date_in TIMESTAMP without time zone
);

INSERT INTO users (login, password, name, lastname) VALUES ('wika', 'wika', 'Wiktoria', 'Poślenicka');
INSERT INTO destination (destination, date_start, date_end, description, longitude, latitude, cost) VALUES ('NewYork', now(), now(), 'Wyjazd pod namiot do USA.', -73.9400, 40.6700, '10 000PLN');
INSERT INTO destination (destination, date_start, date_end, description, longitude, latitude, cost) VALUES ('Chicago', now(), now(), 'Wyjazd na musical.', -87.6278, 41.8819, '5 000PLN');
INSERT INTO destination (destination, date_start, date_end, description, longitude, latitude, cost) VALUES ('Toronto', now(), now(), 'Wyjazd na alaske.', -79.4000, 43.7000, '15 000PLN');
INSERT INTO travel (destination_id, user_id) VALUES (1, 1);
INSERT INTO travel (destination_id, user_id) VALUES (2, 1);
INSERT INTO travel (destination_id, user_id) VALUES (3, 1);
