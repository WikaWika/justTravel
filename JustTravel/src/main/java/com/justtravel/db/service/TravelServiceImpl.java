package com.justtravel.db.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.justtravel.db.dao.ITravelDAO;
import com.justtravel.db.dao.IUserDAO;
import com.justtravel.db.entity.Destination;
import com.justtravel.db.entity.Travel;
import com.justtravel.db.entity.User;
import com.justtravel.dto.DestinationDTO;

@Transactional
@Service("travelService")
public class TravelServiceImpl implements TravelService{
	
	@Autowired
	private ITravelDAO travelDAO;
	
	@Autowired
	private IUserDAO userDAO;

	@Override
	public void saveTravel(Travel travel) {
		travelDAO.saveTravel(travel);
	}

	@Override
	public List<Travel> getAllTravelsByUserId(Long userId) {
		return travelDAO.getAllTravelsByUserId(userId);
	}

	@Override
	public Destination updateDestination(Destination dest) {
		return travelDAO.updateDestination(dest);
	}

	@Override
	public void addNewTravelDTO(DestinationDTO destinationDTO) {
		Destination destination = new Destination();
		destination.setCost(destinationDTO.getCost());
		destination.setDateEnd(destinationDTO.getDateEnd());
		destination.setDateStart(destinationDTO.getDateStart());
		destination.setDescription(destinationDTO.getDescription());
		destination.setCity(destinationDTO.getCity());
		destination.setCountry(destinationDTO.getCountry());
		destination.setLatitude(null);
		destination.setLongitude(null);
		
		User userById = userDAO.getUserById(destinationDTO.getUserId());
		
		Travel travel = new Travel();
		travel.setDateIn(new Date());
		travel.setDestination(destination);
		travel.setUser(userById);
		travel.setDateIn(new Date());
		
		travelDAO.addNewTravel(travel);
	}

	@Override
	public void removeTravel(Long travelId) {
		travelDAO.removeTravel(travelId);
	}

	@Override
	public Travel getTravelByDestinationId(Long destinaionId) {
		return travelDAO.getTravelByDestinationId(destinaionId);
	}



}
