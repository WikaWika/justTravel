package com.justtravel.db.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.justtravel.db.dao.IDestinationDAO;
import com.justtravel.db.entity.Destination;

@Transactional
@Service("destinationService")
public class DestinationServiceImpl implements DestinationService{

	@Autowired
	private IDestinationDAO destinationDAO;

	@Override
	public List<Destination> getAllDestination() {
		return destinationDAO.getAllDestination();
	}

	@Override
	public List<Destination> getLatestTravel(int amount) {
		return destinationDAO.getLatestTravel(amount);
	}
}
