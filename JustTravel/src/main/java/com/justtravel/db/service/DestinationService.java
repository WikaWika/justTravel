package com.justtravel.db.service;

import java.util.List;

import com.justtravel.db.entity.Destination;

public interface DestinationService {
	
	List<Destination> getAllDestination();

	List<Destination> getLatestTravel(int amount);
}
