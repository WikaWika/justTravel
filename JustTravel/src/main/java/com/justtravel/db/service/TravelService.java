package com.justtravel.db.service;

import java.util.List;

import com.justtravel.db.entity.Destination;
import com.justtravel.db.entity.Travel;
import com.justtravel.dto.DestinationDTO;

public interface TravelService {
	
	void saveTravel(Travel travel);
	
	List<Travel> getAllTravelsByUserId(Long userId);

	Destination updateDestination(Destination dest);

	void addNewTravelDTO(DestinationDTO destinationDTO);

	void removeTravel(Long travelId);

	Travel getTravelByDestinationId(Long destinaionId);

}
