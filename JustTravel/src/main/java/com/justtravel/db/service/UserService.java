package com.justtravel.db.service;

import com.justtravel.db.entity.User;

public interface UserService {
	
	User getUserById(Long userId);

}
