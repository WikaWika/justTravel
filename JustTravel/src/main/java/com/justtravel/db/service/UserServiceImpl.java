package com.justtravel.db.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.justtravel.db.dao.IUserDAO;
import com.justtravel.db.entity.User;

@Transactional
@Service("userService")
public class UserServiceImpl implements UserService{
	
	@Autowired
	private IUserDAO userDAO;

	@Override
	public User getUserById(Long userId) {
		return userDAO.getUserById(userId);
	}

}
