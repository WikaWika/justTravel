package com.justtravel.db.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.justtravel.db.entity.User;

@Transactional
@Repository("userDAO")
public class UserDAO implements IUserDAO{
	
	@PersistenceContext	
	private EntityManager entityManager;

	@Override
	public User getUserById(Long id) {
		return entityManager.find(User.class, id);
	}

}
