package com.justtravel.db.dao;

import java.util.List;

import com.justtravel.db.entity.Destination;
import com.justtravel.db.entity.Travel;

public interface ITravelDAO {
	
	List<Travel> getAllTravelsByUserId(Long userId);

	void saveTravel(Travel travel);

	Travel updateTravel(Travel travel);

	Destination updateDestination(Destination dest);

	void addNewTravel(Travel travel);

	void removeTravel(Long travelId);

	Travel getTravelByDestinationId(Long destinaionId);

}
