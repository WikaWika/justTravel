package com.justtravel.db.dao;

import com.justtravel.db.entity.User;

public interface IUserDAO {
	
	User getUserById(Long userId);

}
