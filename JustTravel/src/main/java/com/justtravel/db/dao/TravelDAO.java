package com.justtravel.db.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.justtravel.db.entity.Destination;
import com.justtravel.db.entity.Travel;

@Transactional
@Repository("travelDAO")
public class TravelDAO implements ITravelDAO{
	
	@PersistenceContext	
	private EntityManager entityManager;

	@Override
	public List<Travel> getAllTravelsByUserId(Long userId) {
		Query query = entityManager.createQuery("SELECT t FROM Travel t JOIN t.user u WHERE u.id = :userId");
		query.setParameter("userId", userId);
		return query.getResultList();
	}

	@Override
	public void saveTravel(Travel travel) {
		entityManager.persist(travel);
	}

	@Override
	public Travel updateTravel(Travel travel) {
		return entityManager.merge(travel);
	}

	@Override
	public Destination updateDestination(Destination dest) {
		return entityManager.merge(dest);
	}

	@Override
	public void addNewTravel(Travel travel) {
		entityManager.persist(travel);
	}

	@Override
	public void removeTravel(Long id) {
		Query query = entityManager.createQuery("SELECT t FROM Travel t JOIN t.destination d WHERE d.id = :destId");
		query.setParameter("destId", id);
		List resultList = query.getResultList();
		if (!resultList.isEmpty()) {
			Travel travel = (Travel)resultList.get(0);
			entityManager.remove(travel);
		}
	}

	@Override
	public Travel getTravelByDestinationId(Long destinaionId) {
		Query query = entityManager.createQuery("SELECT t FROM Travel t JOIN t.destination d WHERE d.id = :destId");
		query.setParameter("destId", destinaionId);
		List resultList = query.getResultList();
		if (!resultList.isEmpty()) {
			return (Travel) resultList.get(0);
		}
		return null;
	}

}
