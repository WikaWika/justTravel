package com.justtravel.db.dao;

import java.util.List;

import com.justtravel.db.entity.Destination;

public interface IDestinationDAO {
	
	Destination saveDestination(Destination destination);

	List<Destination> getAllDestination();

	List<Destination> getLatestTravel(int amount);

}
