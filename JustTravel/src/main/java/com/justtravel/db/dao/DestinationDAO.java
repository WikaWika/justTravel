package com.justtravel.db.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.justtravel.db.entity.Destination;

@Transactional
@Repository("destinationDAO")
public class DestinationDAO implements IDestinationDAO{
	
	@PersistenceContext	
	private EntityManager entityManager;

	@Override
	public Destination saveDestination(Destination destination) {
		return entityManager.merge(destination);
	}

	@Override
	public List<Destination> getAllDestination() {
		Query query = entityManager.createQuery("FROM Destination");
		return query.getResultList();
	}

	@Override
	public List<Destination> getLatestTravel(int amount) {
		Query query = entityManager.createQuery("SELECT d FROM Travel t JOIN t.destination d ORDER BY t.dateIn desc");
		query.setMaxResults(amount);
		return query.getResultList();
	}

}
