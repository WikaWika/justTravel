package com.justtravel.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.justtravel.db.entity.Destination;
import com.justtravel.db.service.DestinationService;
import com.justtravel.dto.DestinationMap;
import com.justtravel.dto.Utils;

@RestController
@RequestMapping("/destination")
public class DestinationController {

	@Autowired
	private DestinationService destinationService;
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResponseEntity<List<Destination>> getAllDestination(){
		List<Destination> allDestinations = destinationService.getAllDestination();
		return new ResponseEntity<List<Destination>>(allDestinations, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/map", method = RequestMethod.GET)
	public ResponseEntity<List<DestinationMap>> getAllDestinationMap(){
		List<Destination> allDestinations = destinationService.getAllDestination();
		List<DestinationMap> destinationMaps = allDestinations
			.stream()
			.map(d -> new DestinationMap(d.getId(), d.getCity(), d.getDescription(), d.getLongitude(), d.getLatitude()))
			.collect(Collectors.toList());
		return new ResponseEntity<List<DestinationMap>>(destinationMaps, HttpStatus.OK);
	}

	@RequestMapping(value = "/allcountry", method = RequestMethod.GET)
	public ResponseEntity<List<String>> getAllContry(){
		return new ResponseEntity<List<String>>(Utils.COUNTRY_OF_WORLD, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/latest", method = RequestMethod.GET)
	public ResponseEntity<List<Destination>> getLatestTravel(){
		List<Destination> destinations = destinationService.getLatestTravel(3);
		return new ResponseEntity<List<Destination>>(destinations, HttpStatus.OK);
	}
	
	
	
	
	
}
