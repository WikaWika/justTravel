package com.justtravel.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.justtravel.db.entity.Destination;
import com.justtravel.db.entity.Travel;
import com.justtravel.db.service.TravelService;
import com.justtravel.dto.DestinationDTO;

@RestController
@RequestMapping("/travel")
public class TravelController {

	@Autowired
	private TravelService travelService;
	
	@RequestMapping(value = "/travels/{userId}", 
     method = RequestMethod.GET, 
     produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	@ResponseBody
	public ResponseEntity<List<Travel>> getAllTravelsByUserId(@PathVariable("userId") Long userId) {
		List<Travel> list = travelService.getAllTravelsByUserId(userId);
		return new ResponseEntity<List<Travel>>(list, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/updateTravel", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Destination> updateDestination(@RequestBody Destination destination) {
		Destination updatedTravel = travelService.updateDestination(destination);
		return new ResponseEntity<Destination>(updatedTravel, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void addNewTravel(@RequestBody DestinationDTO destinationDTO) {
		travelService.addNewTravelDTO(destinationDTO);
	}
	
	@RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
	public void removeTravel(@PathVariable(value = "id") Long destinationId) {
		travelService.removeTravel(destinationId);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Travel> getTravelByDestinationId(@PathVariable(value = "id") Long destinaionId){
		Travel travel = travelService.getTravelByDestinationId(destinaionId);
		return new ResponseEntity<Travel>(travel, HttpStatus.OK);
	}
	
	
}
