package com.justtravel.dto;

import java.math.BigDecimal;

public class DestinationMap {
	
	private Long id;
	private String city;
	private String desc;
	private BigDecimal longitude;
	private BigDecimal latitude;
	
	public DestinationMap() {
		super();
	}
	public DestinationMap(Long id, String city, String desc, BigDecimal longitude, BigDecimal latitude) {
		super();
		this.id = id;
		this.city = city;
		this.desc = desc;
		this.longitude = longitude;
		this.latitude = latitude;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public BigDecimal getLongitude() {
		return longitude;
	}
	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}
	public BigDecimal getLatitude() {
		return latitude;
	}
	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}
	
	

}
